from django.db import models


class ModbusDevice(models.Model):
    address = models.CharField(
        max_length=256,
        null=False,
        blank=False,
        help_text="The Host IP Address",
        default="127.0.0.1",
    )
    port = models.CharField(
        max_length=256,
        null=False,
        blank=False,
        help_text="Host Port MODBUS",
        default="502",
    )
    read_coil = models.CharField(
        max_length=256,
        null=False,
        blank=False,
        help_text="Address read Coil",
        default="0",
    )
    good_state = models.CharField(
        max_length=256,
        null=False,
        blank=False,
        help_text="Good State of Check 0 or 1",
        default="0",
    )
    write_coil = models.CharField(
        max_length=256,
        null=True,
        blank=True,
        help_text="Address write Coil",
        default="0",
    )
    write_coil_value = models.CharField(
        max_length=256,
        null=True,
        blank=True,
        help_text="write Coil value",
        default="1",
    )

    class Meta:
        db_table = "modbus_device"
        unique_together = (("address", "port", "read_coil"),)

from django.apps import apps
from django.contrib import admin

for model in apps.get_app_config(
    "django_monstack_modbus_concentrator_models"
).models.values():
    admin.site.register(model)
